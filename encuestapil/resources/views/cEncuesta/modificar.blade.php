@extends('app')
@section('contenedor')
<section class="content">
    <div class="container-fluid">
        <!-- Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            CREAR ENCUESTA
                            <small>Aquí puedes crear tus encuestas :)</small>
                        </h2>
                    </div>
<!--==================================================================================================================================================-->
                    <div class="body">
                        <form action="{{route('cEncuesta.update', $encuestas->id)}}" method="POST">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <br><br>
                                        <input type="text" class="form-control" maxlength="10" placeholder="Nombre de la encuesta" name="nombreEncuesta" value="{{$encuestas->nombreEncuesta}}" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="formFile" class="form-label">Fecha de Inicio</label>
                                        <br>
                                        <br>
                                        <input type="date" class="form-control" maxlength="10" placeholder="Fecha de Inicio" name="fechaInicio" value="{{$encuestas->fechaInicio}}" />
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <label for="formFile" class="form-label">Fecha de Finalización</label>
                                        <br>
                                        <br>
                                        <input type="date" class="form-control" placeholder="Fecha de Finalización" name="fechaFinal" value="{{$encuestas->fechaFinal}}" />
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <br><br>
                                        <input type="text" class="form-control" placeholder="Estado" name="estadoEncuesta" value="{{$encuestas->estadoEncuesta}}"/>
                                    </div>
                                </div>
                            </div>
                        </div>


                        {{csrf_field()}}
                        {{method_field('PUT')}}
                            <div class="container-fluid" align="center">
        
                                
                                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    <input type="submit" href="#" class="btn bg-green"  > <i class="material-icons">save</i>
                                </div>
                                
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Input -->
    </div>
</section>
@endsection