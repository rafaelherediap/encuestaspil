@extends('app')
@section('contenedor')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Listado de Encuesta
                            <!--<small>Add <code>.table-hover</code> to enable a hover state on table rows within a <code>&lt;tbody&gt;</code>.</small>-->
                        </h2>
                    </div>
                    <div class="body table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>CÓDIGO</th>
                                    <th>NOMBRE</th>
                                    <th>FECHA INICIO</th>
                                    <th>FECHA FINAL</th>
                                    <th>ESTADO</th>
                                    <th>ACCIÓN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($encuestas as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->nombreEncuesta}}</td>
                                    <td>{{$item->fechaInicio}}</td>
                                    <td>{{$item->fechaFinal}}</td>
                                    <td>{{$item->estadoEncuesta}}</td>
                                    
                                    <td>
                                        <form  action="{{route('des',$item->id)}}" method="POST">
                                        <a type="button" href="{{route('cEncuesta.edit', $item->id)}}" class="btn bg-blue ">
                                            Modificar
                                        </a>
                                        {{{ csrf_field()}}}
                                        {{method_field('PUT')}}
                                        @if ($item->estadoEncuesta!=1)
                                        <input type="hidden" value="0" name="status">
                                        <input type="submit" class="btn btn-success"value="Activar">
                                        @else
                                        <input type="hidden" value="1" name="status">
                                        <input type="submit" class="btn btn-danger"value="Eliminar">
                                        @endif
                                        </form>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection