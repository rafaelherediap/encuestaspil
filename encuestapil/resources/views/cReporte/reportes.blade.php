@extends('app')
@section('contenedor')

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-light-green hover-zoom-effect">
                         <a href="#"><div class="icon">
                            <i class="material-icons">supervisor_account</i>
                        </div></a>
                        <div class="content">
                            <a href="#"><div class="text">REPORTE POR EDAD Y SEXO</div></a>
                            <div class="number">3</div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box-2 bg-blue hover-zoom-effect">
                        <a href="#"><div class="icon">
                            <i class="material-icons">person_pin</i>
                        </div></a>
                        <div class="content">
                            <a href="#"><div class="text">REPORTE PERSONALIZADO</div></a>
                            <div class="number">6</div>
                        </div>
                    </div>
                </div>
            </div>

@endsection