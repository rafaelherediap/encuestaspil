@extends('app')
@section('contenedor')

<!--=============================================================CONTENIDO DE LA PÁGINA =============================================================-->
<section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Listado de Usuario
                                <!--<small>Add <code>.table-hover</code> to enable a hover state on table rows within a <code>&lt;tbody&gt;</code>.</small>-->
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>NOMBRES</th>
                                        <th>APELLIDOS</th>
                                        <th>E-mail</th>
                                                                   
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Patricia</td>
                                        <td>Castro Landivar</td>
                                        <td>castrolandivarpatricia@gmail.com</td>
                                       
                                        <td>
                                            <a type="button" href="ActualizarClient.html" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">autorenew</i>
                                            </a>
                                            <a type="button" href="#" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Giordany</td>
                                        <td>Aguilera Frias</td>
                                        <td>aguileragiordany@gmail.com</td>
                                       
                                        <td>
                                            <a type="button" href="ActualizarClient.html" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">autorenew</i>
                                            </a>
                                            <a type="button" href="#" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Rafael</td>
                                        <td>Heredia Padilla</td>
                                        <td>herediarafael@gmail.com</td>
                                                                            
                                        <td>
                                            <a type="button" href="ActualizarClient.html" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">autorenew</i>
                                            </a>
                                            <a type="button" href="#" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--=============================================================FIN DE LA PÁGINA =============================================================-->



@endsection