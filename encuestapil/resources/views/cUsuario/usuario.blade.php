@extends('app')

@section('contenedor')


<!--=============================================================CONTENIDO DE LA PÁGINA =============================================================-->

    <section class="content">
        <div class="container-fluid">
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                REGISTRO DE USUARIO
                                <small>Registra cualquier usuario en el sistema </small>
                            </h2>
                        </div>
<!--==================================================================================================================================================-->
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Nombres" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Apellidos" />
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--==================================================================================================================================================-->
                            <div class="row clearfix">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="E-mail" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control" placeholder="Contraseña" />
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--==================================================================================================================================================-->
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea rows="4" class="form-control no-resize" placeholder="Confirmar Contraseña"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--==================================================================================================================================================-->
                               <!-- <div class="container-fluid" align="center">
                                            <a type="button" href="#" class="btn bg-red waves-effect">
                                                <i class="material-icons">clear</i>
                                                <span>LIMPIAR</span>
                                            </a>

                                            <a type="button" href="#" class="btn bg-light-green waves-effect">
                                                <span>GUARDAR</span>
                                                <i class="material-icons">save</i>
                                            </a>
                                </div>-->
                                <div class="container-fluid" align="center">
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <a type="button" href="#" class="btn bg-red"><i class="material-icons">cancel</i> LIMPIAR </a>
                                    </div>

                                   
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <a type="button" href="#" class="btn bg-green"> GUARDAR <i class="material-icons">save</i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
        </div>
    </section>


@endsection