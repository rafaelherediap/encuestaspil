@extends('app')
@section('contenedor')
<section class="content">
        <div class="container-fluid">
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                REGISTRO DE PREGUNTAS
                                <small>Registro de pregunstas y las alternativas </small>
                            </h2>
                        </div>
<!--==================================================================================================================================================-->
                        <div class="body">
                            <div class="row clearfix">
                                
                                <div class="col-sm-6">
                                   
                                    <div class="form-group">
                                        <select class="form-select form-select-sm" aria-label=".form-select-sm example">
                                            <option selected>Seleccione una encuesta</option>
                                            <option value="1">E1</option>
                                            <option value="2">E2</option>
                                            <option value="3">E3</option>
                                        </select>
                                        
                                        <div class="form-line">
                                            Pregunta<input type="text" class="form-control" maxlength="50" placeholder="Pregunta" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            Opción 1<input type="text" class="form-control" maxlength="30" placeholder="Opción 1" />
                                        </div>
                                        <div class="form-line">
                                            Opción 2<input type="text" class="form-control" maxlength="30" placeholder="Opción 2" />
                                        </div>
                                        <div class="form-line">
                                            Opción 3<input type="text" class="form-control" maxlength="30" placeholder="Opción 3" />
                                        </div>
                                    </div>
                                </div>

                            </div>

<!--==================================================================================================================================================-->
                               
                                <div class="container-fluid" align="center">
                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <a type="button" href="#" class="btn bg-red"><i class="material-icons">cancel</i> LIMPIAR </a>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                                        <a type="button" href="#" class="btn bg-green"> GUARDAR <i class="material-icons">save</i></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Input -->
        </div>
    </section>
@endsection