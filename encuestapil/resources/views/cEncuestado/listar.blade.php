@extends ('app')
@section ('contenedor')

<!--=============================================================CONTENIDO DE LA PÁGINA =============================================================-->
     <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Listado de Encuestantes
                                <!--<small>Add <code>.table-hover</code> to enable a hover state on table rows within a <code>&lt;tbody&gt;</code>.</small>-->
                            </h2>
                        </div>
                        <div class="body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>CÉDULA</th>
                                        <th>NOMBRES</th>
                                        <th>APELLIDOS</th>
                                        <th>TELÉFONO</th>
                                        <th>DIRECCIÓN</th>
                                        <th>DESCRIPCIÓN</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1600868645</td>
                                        <td>Walter Andrés</td>
                                        <td>Benítez Martínez</td>
                                        <td>0991253468</td>
                                        <td>Av. Los Cusis</td>
                                        <td>Excelente Trabajado</td>
                                        <td>
                                            <a type="button" href="ActualizarClient.html" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">autorenew</i>
                                            </a>
                                            <a type="button" href="#" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>0712352130</td>
                                        <td>Carla  Andrea</td>
                                        <td>Cevallos Durán</td>
                                        <td>0991231231</td>
                                        <td>Calle Independencia</td>
                                        <td>Mejoras en el producto</td>
                                        <td>
                                            <a type="button" href="ActualizarClient.html" class="btn bg-blue btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">autorenew</i>
                                            </a>
                                            <a type="button" href="#" class="btn bg-red btn-circle waves-effect waves-circle waves-float">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--=============================================================FIN DE LA PÁGINA =============================================================-->



@endsection