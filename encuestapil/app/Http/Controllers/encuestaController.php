<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Encuesta;

class encuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $encuestas= Encuesta::get();
        
        return view('cEncuesta.inicio', compact('encuestas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cEncuesta.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $encuestas = Encuesta::create([
            'nombreEncuesta'=> $request->nombreEncuesta,
                'fechaInicio'=> $request->fechaInicio,
                'fechaFinal' => $request->fechaFinal,
                'estadoEncuesta'=>$request->estadoEncuesta,
                
        ]);
        $encuestas= Encuesta::get();

        return view('cEncuesta.inicio', compact('encuestas'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $encuestas= Encuesta::findOrfail($id);
        return view('cEncuesta.modificar', compact('encuestas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $encuestas= Encuesta::find($id)->update($request->all());
        $encuestas= Encuesta::get();
        return view('cEncuesta.inicio', compact('encuestas'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function desactivar(Request $request, Encuesta $encuesta)
    {   
        // dd($camisa);
        if($request->status==1){
            $encuesta->estadoEncuesta=0;
            $encuesta->save();
        }
        else{
            $encuesta->estadoEncuesta=1;
            $encuesta->save();
        }
       
        return redirect()->route('cEncuesta.index'); 
        
    
         
     }
    
}
