<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Encuesta extends Model
{
    protected $fillable = ['nombreEncuesta','fechaInicio','fechaFinal','estadoEncuesta'];
}
