<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\encuestaController;
use App\Http\Controllers\encuestadoController;
use App\Http\Controllers\usuarioController;
use App\Http\Controllers\preguntaController;
use App\Http\Controllers\reporteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});
Route::get('inicio/', function () {
    return view('index');
});
Route::put('cEncuesta2/{encuesta}',[encuestaController::class,'desactivar'])->name('des');
Route::resource('cEncuesta',encuestaController::class);
Route::resource('cEncuestado',encuestadoController::class);
Route::resource('cUsuario',usuarioController::class);
Route::resource('cPregunta',preguntaController::class);
Route::resource('cReporte',reporteController::class);
